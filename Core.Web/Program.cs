﻿using Core.DAL;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Core.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            DbContextConfig.InitSeed(host);

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}

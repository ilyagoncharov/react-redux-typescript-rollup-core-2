﻿using System;
using Core.Web.Models;
using Core.Web.Providers;
using Microsoft.AspNetCore.Mvc;

namespace Core.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/token")]
    public class TokenController : Controller
    {
        private OAuthProvider OAuthProvider { get; }

        public TokenController(OAuthProvider oAuthProvider)
        {
            OAuthProvider = oAuthProvider;
        }

        [HttpPost]
        public OperationResult Token([FromBody] JwtRequest parameters)
        {
            System.Threading.Thread.Sleep(2000);

            if (parameters == null)
            {
                return new OperationResult
                {
                    Success = false,
                    Message = "null of parameters"
                };
            }

            try
            {
                switch (parameters.GrantType)
                {
                    case "password":
                    {
                        return OAuthProvider.DoPassword(parameters);
                    }
                    case "refresh_token":
                    {
                        return OAuthProvider.DoRefreshToken(parameters);
                    }
                    default:
                        return new OperationResult
                        {
                            Success = false,
                            Message = "bad request"
                        };
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
import { all }            from 'redux-saga/effects';
import { SagaMiddleware } from 'redux-saga';
import { authSagas }      from './authentication/sagas';
import { userSagas }      from './user/sagas';

function* rootSaga()
{
  yield all([
    ...authSagas,
    ...userSagas
  ]);
}

export const sagasRun = (sagaMiddleware: SagaMiddleware<object>) =>
{
  sagaMiddleware.run(rootSaga);
};

import { Dispatch, Store }      from 'redux';
import { IAppState }            from './state';
import { push }                 from 'react-router-redux';
import { IAuthenticationState } from './authentication/reducers';

let store: any;

export function setStore(appStore: any)
{
  store = appStore;
}

export class AppStore
{
  public static get store(): Store<IAppState>
  {
    return store;
  }

  public static get dispatch(): Dispatch
  {
    return store.dispatch;
  }

  public static routeNavigate(url: string): void
  {
    store.dispatch(push(url));
  }

  public static get auth(): IAuthenticationState
  {
    return store.getState().authentication;
  }
}
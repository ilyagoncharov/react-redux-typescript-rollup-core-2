import { authenticationReducer, IAuthenticationState } from './authentication/reducers';
import { combineReducers, Reducer }                    from 'redux';
import { IUserState, userReducer }                     from './user/reducers';
import { routerReducer, RouterState }                  from 'react-router-redux';

export interface IAppState
{
  routing: RouterState;
  authentication: IAuthenticationState;
  usersState: IUserState;
}

export const reducers: Reducer<IAppState> = combineReducers<IAppState>({
  routing: routerReducer,
  authentication: authenticationReducer,
  usersState: userReducer
});

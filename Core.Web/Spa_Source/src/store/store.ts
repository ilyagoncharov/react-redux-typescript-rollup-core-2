import createSagaMiddleware                                from 'redux-saga';
import { applyMiddleware, createStore, Middleware, Store } from 'redux';
import { IAppState, reducers }                             from './state';
import createHistory                                       from 'history/createBrowserHistory';
import { routerMiddleware }                                from 'react-router-redux';
import { composeMiddleware }                               from './common/middleware';
import { sagasRun }                                        from './sagas';
import { setStore }                                        from './app-store';

export const browserHistory = createHistory();
const sagaMiddleware = createSagaMiddleware();

const configureStore = (): Store<IAppState> =>
{
  const middlewares: Middleware[] = [
    routerMiddleware(browserHistory),
    sagaMiddleware
  ];

  return createStore(
    reducers,
    composeMiddleware(applyMiddleware(...middlewares))
  );
};

const store = configureStore();

setStore(store);
sagasRun(sagaMiddleware);

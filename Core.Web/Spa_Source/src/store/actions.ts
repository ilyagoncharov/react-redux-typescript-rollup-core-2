import { ActionUnionType } from './common/actions';
import { AuthActions }     from './authentication/actions';
import { UserActions }     from './user/actions';

export type RootAction = ActionUnionType<typeof AuthActions>
  | ActionUnionType<typeof UserActions>;

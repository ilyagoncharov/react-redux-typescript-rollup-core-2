import { httpRequestFactory }                    from '../common/http-factory';
import { httpRequest, HttpRequestMethod }        from '../../services/http.service';
import { cloneableGenerator, SagaIteratorClone } from 'redux-saga/utils';
import { call, put, select, take }               from 'redux-saga/effects';
import { AuthActions, AuthActionType }           from '../authentication/actions';
import { selectAuthentication }                  from '../common/sagas-selects';
import axios                                     from 'axios';

describe('HttpRequestFactory', () =>
{
  const testHttpRequest = (clone: SagaIteratorClone) =>
  {
    const token = axios.CancelToken.source();
    const request = call(httpRequest, 'test', HttpRequestMethod.Get, null, token.token);
    expect(clone.next(token).value).toEqual(request);
  };

  const httpRequestTest = httpRequestFactory('test', HttpRequestMethod.Get);
  const generator = cloneableGenerator(() => httpRequestTest.generator(null))();

  it('Select auth', () =>
  {
    expect(generator.next().value).toEqual(select(selectAuthentication));
  });

  it('refreshTokenRequestLoading false', () =>
  {
    const clone = generator.clone();

    const auth = {
      refreshTokenRequestLoading: false
    };

    clone.next(auth);

    testHttpRequest(clone);
  });

  it('refreshTokenRequestLoading true, HttpRequest success', () =>
  {
    const auth = {
      refreshTokenRequestLoading: true
    };

    expect(generator.next(auth).value).toEqual(take(AuthActionType.AUTH_REFRESH_TOKEN_END));

    generator.next();

    testHttpRequest(generator);
  });

  it('HttpRequest error 401', () =>
  {
    const clone = generator.clone();

    const error = {
      config: '',
      response: {
        status: 401
      }
    };

    expect(clone.throw(error).value).toEqual(put(AuthActions.httpRefreshToken()));

    expect(clone.next().value).toEqual(take(AuthActionType.AUTH_REFRESH_TOKEN_END));

    expect(clone.next().value).toEqual(select(selectAuthentication));

    clone.next(true);

    testHttpRequest(clone);
  });
});

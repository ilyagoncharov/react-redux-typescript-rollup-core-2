import { Action }                           from 'redux';
import { actionFactory, ActionWithPayload } from '../common/actions';
import { UserDetails }                      from '../../models/dto.models';

export enum UserActionType
{
  USER_HTTP_LIST              = 'USER_HTTP_LIST',
  USER_ADD_LIST               = 'USER_ADD_LIST',
  USER_HTTP_CHANGE_STATUS     = 'USER_HTTP_CHANGE_STATUS',
  USER_HTTP_CHANGE_STATUS_END = 'USER_HTTP_CHANGE_STATUS_END',
  USER_CHANGE_STATUS          = 'USER_CHANGE_STATUS',
  USER_CLEAR                  = 'USER_CLEAR'
}

export class UserHttpUpdate
{
  public id: number;
  public isActive: boolean;
}

export type UserHttpListAction = Action<UserActionType.USER_HTTP_LIST>;
export type UserAddListAction = ActionWithPayload<UserActionType.USER_ADD_LIST, UserDetails[]>;
export type UserHttpChangeStatusAction = ActionWithPayload<UserActionType.USER_HTTP_CHANGE_STATUS, UserHttpUpdate>;
export type UserHttpChangeStatusEndAction = Action<UserActionType.USER_HTTP_CHANGE_STATUS_END>;
export type UserChangeStatusAction = ActionWithPayload<UserActionType.USER_CHANGE_STATUS, UserDetails>;
export type UserClearAction = Action<UserActionType.USER_CLEAR>;

const httpList = actionFactory<UserHttpListAction>(UserActionType.USER_HTTP_LIST);
const addList = actionFactory<UserAddListAction>(UserActionType.USER_ADD_LIST);
const httpChangeStatus = actionFactory<UserHttpChangeStatusAction>(UserActionType.USER_HTTP_CHANGE_STATUS);
const httpChangeStatusEnd = actionFactory<UserHttpChangeStatusEndAction>(UserActionType.USER_HTTP_CHANGE_STATUS_END);
const changeStatus = actionFactory<UserChangeStatusAction>(UserActionType.USER_CHANGE_STATUS);
const clear = actionFactory<UserClearAction>(UserActionType.USER_CLEAR);

export const UserActions = {
  httpList: httpList,
  addList: addList,
  httpChangeStatus: httpChangeStatus,
  httpChangeStatusEnd: httpChangeStatusEnd,
  changeStatus: changeStatus,
  clear: clear
};

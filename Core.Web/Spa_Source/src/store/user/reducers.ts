import { UserDetails }    from '../../models/dto.models';
import { RootAction }     from '../actions';
import { UserActionType } from './actions';

export interface IUserState
{
  users: UserDetails[],
  changeStatusLoading: boolean
}

const init: IUserState = {
  users: [],
  changeStatusLoading: false
};

export const userReducer = (state: IUserState = init, action: RootAction): IUserState =>
{
  switch (action.type)
  {
    case UserActionType.USER_ADD_LIST:
      return {...state, users: action.payload};
    case UserActionType.USER_HTTP_CHANGE_STATUS:
      return {...state, changeStatusLoading: true};
    case UserActionType.USER_CHANGE_STATUS:
      return {
        ...state,
        users: state.users.map((user: UserDetails) =>
        {
          if (user.id === action.payload.id)
          {
            user.isActive = action.payload.isActive;
          }
          return user;
        })
      };
    case UserActionType.USER_HTTP_CHANGE_STATUS_END:
      return {...state, changeStatusLoading: false};
    case UserActionType.USER_CLEAR:
      return {...state, users: []};
    default:
      return state;
  }
};

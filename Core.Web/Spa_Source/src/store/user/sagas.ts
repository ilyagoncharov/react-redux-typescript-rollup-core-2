import { takeChannel }                                                             from '../common/sagas';
import { call, put }                                                               from 'redux-saga/effects';
import { UserActions, UserActionType, UserHttpChangeStatusAction, UserHttpUpdate } from './actions';
import { UserDetails }                                                             from '../../models/dto.models';
import { HttpRequestMethod }                                                       from '../../services/http.service';
import { httpRequestFactory }                                                      from '../common/http-factory';

const userGetListRequest = httpRequestFactory<UserDetails[], never>('data/users', HttpRequestMethod.Get);
const userUpdateRequest = httpRequestFactory<UserDetails, UserHttpUpdate>('data/update', HttpRequestMethod.Post);

export function* getUsersSaga()
{
  try
  {
    const data: UserDetails[] = userGetListRequest.call(yield call(userGetListRequest.generator));

    yield put(UserActions.addList(data));
  }
  catch (e)
  {
    console.log('e', e);
  }
}

function* changeStatusUserSaga(action: UserHttpChangeStatusAction)
{
  try
  {
    const data: UserDetails = userUpdateRequest.call(yield call(userUpdateRequest.generator, action.payload));

    yield put(UserActions.changeStatus(data));
  }
  catch (e)
  {
    console.log('e', e);
  }
  finally
  {
    yield put(UserActions.httpChangeStatusEnd());
  }
}

export const userSagas = [
  takeChannel(UserActionType.USER_HTTP_LIST, getUsersSaga),
  takeChannel(UserActionType.USER_HTTP_CHANGE_STATUS, changeStatusUserSaga)
];

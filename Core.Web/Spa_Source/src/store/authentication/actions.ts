import { actionFactory, ActionWithPayload } from '../common/actions';
import { Authentication }                   from '../../models/dto.models';
import { Action }                           from 'redux';

export enum AuthActionType
{
  AUTH_HTTP_LOG_IN         = 'AUTH_HTTP_LOG_IN',
  AUTH_LOG_IN_SUCCESS      = 'AUTH_LOG_IN_SUCCESS',
  AUTH_LOG_IN_ERROR        = 'AUTH_LOG_IN_ERROR',
  AUTH_LOG_OUT             = 'AUTH_LOG_OUT',
  AUTH_HTTP_REFRESH_TOKEN  = 'AUTH_HTTP_REFRESH_TOKEN',
  AUTH_REFRESH_TOKEN_START = 'AUTH_REFRESH_TOKEN_START',
  AUTH_REFRESH_TOKEN_END   = 'AUTH_REFRESH_TOKEN_END'
}

export class AuthHttpLogIn
{
  public email: string;
  public password: string;
}

export type AuthHttpLogInAction = ActionWithPayload<AuthActionType.AUTH_HTTP_LOG_IN, AuthHttpLogIn>;
export type AuthLogInSuccessAction = ActionWithPayload<AuthActionType.AUTH_LOG_IN_SUCCESS, Authentication>;
export type AuthLogInErrorAction = Action<AuthActionType.AUTH_LOG_IN_ERROR>;
export type AuthLogOutAction = Action<AuthActionType.AUTH_LOG_OUT>;

export type AuthHttpRefreshToken = Action<AuthActionType.AUTH_HTTP_REFRESH_TOKEN>;
export type AuthRefreshTokenStart = Action<AuthActionType.AUTH_REFRESH_TOKEN_START>;
export type AuthRefreshTokenEnd = Action<AuthActionType.AUTH_REFRESH_TOKEN_END>;

const httpLogIn = actionFactory<AuthHttpLogInAction>(AuthActionType.AUTH_HTTP_LOG_IN);
const logInSuccess = actionFactory<AuthLogInSuccessAction>(AuthActionType.AUTH_LOG_IN_SUCCESS);
const logInError = actionFactory<AuthLogInErrorAction>(AuthActionType.AUTH_LOG_IN_ERROR);
const logOut = actionFactory<AuthLogOutAction>(AuthActionType.AUTH_LOG_OUT);

const httpRefreshToken = actionFactory<AuthHttpRefreshToken>(AuthActionType.AUTH_HTTP_REFRESH_TOKEN);
const refreshTokenStart = actionFactory<AuthRefreshTokenStart>(AuthActionType.AUTH_REFRESH_TOKEN_START);
const refreshTokenEnd = actionFactory<AuthRefreshTokenEnd>(AuthActionType.AUTH_REFRESH_TOKEN_END);

export const AuthActions = {
  httpLogIn: httpLogIn,
  logInSuccess: logInSuccess,
  logInError: logInError,
  logOut: logOut,
  httpRefreshToken: httpRefreshToken,
  refreshTokenStart: refreshTokenStart,
  refreshTokenEnd: refreshTokenEnd
};

import { call, put, takeEvery }                                            from 'redux-saga/effects';
import { Authentication, JwtRequest, OperationResult }                     from '../../models/dto.models';
import { AuthActions, AuthActionType, AuthHttpLogIn, AuthHttpLogInAction } from './actions';
import { showError }                                                       from '../../library/error';
import { takeChannel }                                                     from '../common/sagas';
import { HttpRequestMethod }                                               from '../../services/http.service';
import { authRequestFactory }                                              from '../common/http-factory';
import { Page }                                                            from '../../routing/page';
import { AppStore }                                                        from '../app-store';
import { push }                                                            from 'react-router-redux';

const authenticateRequest = authRequestFactory<OperationResult<Authentication>, JwtRequest>('token', HttpRequestMethod.Post);

function* authenticate(userLogin: AuthHttpLogIn)
{
  const request = new JwtRequest();
  request.grantType = 'password';
  request.username = userLogin.email;
  request.password = userLogin.password;

  const data: OperationResult<Authentication> = authenticateRequest.call(yield call(authenticateRequest.generator, request));

  if (data.success)
  {
    yield put(AuthActions.logInSuccess(data.object));

    yield put(push(Page.requests.path));
  }
}

export function* refreshToken()
{
  const auth = AppStore.auth;

  if (!auth.auth)
  {
    yield put(push(Page.login.path));
  }

  yield put(AuthActions.refreshTokenStart());

  try
  {
    const request = new JwtRequest();
    request.grantType = 'refresh_token';
    request.username = auth.auth.user.email;
    request.refreshToken = auth.auth.refreshToken;

    const data: OperationResult<Authentication> = authenticateRequest.call(yield call(authenticateRequest.generator, request));

    if (data.success)
    {
      yield put(AuthActions.logInSuccess(data.object));
    }
    else
    {
      yield put(AuthActions.logOut());
    }

    return data;
  }
  catch (e)
  {
    yield put(AuthActions.logOut());
  }
  finally
  {
    yield put(AuthActions.refreshTokenEnd());
  }
}

function* logInSaga(action: AuthHttpLogInAction)
{
  try
  {
    yield call(authenticate, action.payload);
  }
  catch (e)
  {
    showError(e);

    yield put(AuthActions.logInError());
  }
}

function* logOut()
{
  yield put(push(Page.login.path));
}

export const authSagas = [
  takeChannel(AuthActionType.AUTH_HTTP_REFRESH_TOKEN, refreshToken, 0),
  takeEvery(AuthActionType.AUTH_HTTP_LOG_IN, logInSaga),
  takeEvery(AuthActionType.AUTH_LOG_OUT, logOut)
];

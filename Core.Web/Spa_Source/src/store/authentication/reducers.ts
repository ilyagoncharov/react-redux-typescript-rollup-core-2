import { AuthActionType } from './actions';
import { Authentication } from '../../models/dto.models';
import { AuthService }    from '../../services/auth.service';
import { RootAction }     from '../actions';

export interface IAuthenticationState
{
  auth: Authentication;
  isAuthenticated: boolean;
  firstRefreshTokenRequestEnd: boolean;
  refreshTokenRequestLoading: boolean;
}

const init: IAuthenticationState = {
  auth: AuthService.auth,
  isAuthenticated: false,
  firstRefreshTokenRequestEnd: false,
  refreshTokenRequestLoading: false
};

export const authenticationReducer = (state: IAuthenticationState = init, action: RootAction): IAuthenticationState =>
{
  switch (action.type)
  {
    case AuthActionType.AUTH_LOG_IN_SUCCESS:
      AuthService.initialize(action.payload);
      return {...state, auth: action.payload, isAuthenticated: true};
    case AuthActionType.AUTH_LOG_IN_ERROR:
      return {...state, auth: null, isAuthenticated: false};
    case AuthActionType.AUTH_LOG_OUT:
      return {...state, auth: null, isAuthenticated: false};
    case AuthActionType.AUTH_REFRESH_TOKEN_START:
      return {...state, refreshTokenRequestLoading: true};
    case AuthActionType.AUTH_REFRESH_TOKEN_END:
      return {...state, refreshTokenRequestLoading: false, firstRefreshTokenRequestEnd: true};
    default:
      return state;
  }
};

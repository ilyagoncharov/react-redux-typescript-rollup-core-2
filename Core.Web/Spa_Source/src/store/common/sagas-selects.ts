import { IAppState }            from '../state';
import { IAuthenticationState } from '../authentication/reducers';

export function selectAuthentication(state: IAppState): IAuthenticationState
{
  return state.authentication;
}

import { HttpRequestMethod, httpRequest }             from '../../services/http.service';
import axios, { AxiosError, CancelTokenSource }       from 'axios';
import { call, cancel, cancelled, put, select, take } from 'redux-saga/effects';
import { AuthActions, AuthActionType }                from '../authentication/actions';
import { getAxiosError, showError }                   from '../../library/error';
import { selectAuthentication }                       from './sagas-selects';

export interface IRequestGenerator<T, B>
{
  generator: (body?: B) => IterableIterator<any>;
  call: (args: any) => T;
}

export const httpRequestFactory = <T, B>(url: string, method: HttpRequestMethod): IRequestGenerator<T, B> =>
{
  const generator = function* (body?: B)
  {
    let cancelTokenSource: CancelTokenSource;

    try
    {
      const auth = yield select(selectAuthentication);

      if (auth.refreshTokenRequestLoading)
      {
        yield take(AuthActionType.AUTH_REFRESH_TOKEN_END);
      }

      try
      {
        cancelTokenSource = yield axios.CancelToken.source();
        return yield call(httpRequest, url, method, body, cancelTokenSource.token);
      }
      catch (e)
      {
        const error: AxiosError = getAxiosError(e);

        if (error.response.status === 401)
        {
          yield put(AuthActions.httpRefreshToken());

          yield take(AuthActionType.AUTH_REFRESH_TOKEN_END);

          const auth = yield select(selectAuthentication);

          if (auth)
          {
            cancelTokenSource = yield axios.CancelToken.source();
            return yield call(httpRequest, url, method, body, cancelTokenSource.token);
          }
        }
        else
        {
          showError(e);
        }

        throw e;
      }
    }
    finally
    {
      if (yield cancelled())
      {
        if (cancelTokenSource)
        {
          cancelTokenSource.cancel();
        }
      }
    }
  };

  return {
    generator: generator,
    call: a => a
  };
};

export const authRequestFactory = <T, B>(url: string, method: HttpRequestMethod): IRequestGenerator<T, B> =>
{
  const generator = function* (body?: B)
  {
    return yield call(() => httpRequest<T>(url, method, body));
  };

  return {
    generator: generator,
    call: a => a
  };
};

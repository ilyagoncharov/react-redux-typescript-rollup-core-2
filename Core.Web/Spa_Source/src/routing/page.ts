export interface IPage
{
  path: string;
}

export const PageDefault: IPage = {
  path: '/'
};

export const PageAuthLayout: IPage = {
  path: '/auth'
};

export const PageLogin: IPage = {
  path: PageAuthLayout.path + '/login'
};

export const PageRegister: IPage = {
  path: PageAuthLayout.path + '/register'
};

export const PageMainLayout: IPage = {
  path: '/main'
};

export const PageRequests: IPage = {
  path: PageMainLayout.path + '/requests'
};

export const PageUsers: IPage = {
  path: PageMainLayout.path + '/users'
};

export class Page
{
  public static readonly default: IPage = PageDefault;

  public static readonly authLayout: IPage = PageAuthLayout;
  public static readonly login: IPage = PageLogin;
  public static readonly register: IPage = PageRegister;

  public static readonly mainLayout: IPage = PageMainLayout;
  public static readonly requests: IPage = PageRequests;
  public static readonly users: IPage = PageUsers;
}

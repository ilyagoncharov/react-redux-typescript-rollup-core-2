import * as React                               from 'react';
import { Redirect, Route, RouteComponentProps } from 'react-router';
import { Page }                                 from './page';
import { connect }                              from 'react-redux';
import { IAppState }                            from '../store/state';
import { GetProps }                             from '../app/types';

interface IStateProps
{
  isAuthenticated: boolean;
}

interface IRouteProps
{
  path: string;
  component?: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>;
}

type IProps = GetProps<IStateProps, IRouteProps>

const PrivateRoute = ({component: Component, isAuthenticated, ...rest}: IProps) => (
  <Route {...rest} render={(props) =>
  {
    return (
      (isAuthenticated)
        ? <Component {...props} />
        : <Redirect to={{
          pathname: Page.login.path,
          state: {from: props.location}
        }}/>
    )
  }}/>
);

const mapStateToProps = (state: IAppState): IStateProps =>
{
  return {
    isAuthenticated: state.authentication.isAuthenticated
  }
};

export const PrivateRouteConnect = connect(mapStateToProps)(PrivateRoute);

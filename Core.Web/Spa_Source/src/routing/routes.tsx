import * as React                  from 'react';
import { Redirect, Route, Switch } from 'react-router';
import { Page }                    from './page';
import { PrivateRouteConnect }     from './private-route';
import { AuthLayout }              from '../app/auth/auth-layout';
import { MainLayoutConnect }       from '../app/main/main-layout';
import { LoginConnect }            from '../app/auth/login';
import { Register }                from '../app/auth/register';
import { Requests }                from '../app/main/views/requests';
import { UsersConnect }            from '../app/main/views/users';

export const AppRoutes = () => (
  <Switch>
    <Route exact path={Page.default.path} render={() => (<Redirect to={Page.requests.path}/>)}/>
    <Route path={Page.authLayout.path} component={AuthLayout}/>
    <PrivateRouteConnect path={Page.mainLayout.path} component={MainLayoutConnect}/>
  </Switch>
);

export const AuthRoutes = () => (
  <Switch>
    <Route exact path={Page.login.path} component={LoginConnect}/>
    <Route exact path={Page.register.path} component={Register}/>
  </Switch>
);

export const MainRoutes = () => (
  <Switch>
    <Route exact path={Page.requests.path} component={Requests}/>
    <Route exact path={Page.users.path} component={UsersConnect}/>
  </Switch>
);

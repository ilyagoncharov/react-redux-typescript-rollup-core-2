import axios, { AxiosError, AxiosRequestConfig, AxiosResponse, CancelToken } from 'axios';
import { AppStore }                                                          from '../store/app-store';
import { select }                                                            from 'redux-saga/effects';
import { selectAuthentication }                                              from '../store/common/sagas-selects';
import { Authentication }                                                    from '../models/dto.models';
import { IAuthenticationState }                                              from '../store/authentication/reducers';

export enum HttpRequestMethod
{
  Get     = 'GET',
  Post    = 'POST',
  Put     = 'PUT',
  Delete  = 'DELETE',
  Options = 'OPTIONS',
  Head    = 'HEAD',
  Patch   = 'PATH',
}

export interface HttpResult<T>
{
  httpRequest: Promise<AxiosResponse>
}

export function* httpRequest<T>(url: string, method: HttpRequestMethod, body?: any, token?: CancelToken)
{
  const requestConfig: AxiosRequestConfig = {
    url: 'http://localhost:57910/api/' + url,
    method: method,
    cancelToken: token
  };

  const auth: IAuthenticationState = yield select(selectAuthentication);

  if (auth && auth.auth && auth.auth.accessToken)
  {
    requestConfig.headers = {
      Authorization: `Bearer ${auth.auth.accessToken}`
    }
  }

  switch (method)
  {
    case HttpRequestMethod.Get:
      requestConfig.params = body;
      break;
    case HttpRequestMethod.Post:
      requestConfig.data = body;
      break;
    default:
      throw new Error('Unknown method.');
  }

  const httpRequest = axios.create().request<T>(requestConfig)
    .then((response: AxiosResponse) => response.data)
    .catch((error: AxiosError) =>
    {
      if (error.response == null && error.message === 'Network Error')
      {
        console.error('Network Error');
      }
      throw error;
    });

  return yield httpRequest;
}

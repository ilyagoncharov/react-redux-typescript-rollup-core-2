import * as React          from 'react';
import ReactDOM            from 'react-dom';
import { Provider }        from 'react-redux';
import { Route }           from 'react-router';
import { ConnectedRouter } from 'react-router-redux';
import { AppStore }        from './store/app-store';
import { browserHistory }  from './store/store';
import { AppConnect }      from './app/app';
import './polyfills';

ReactDOM.render(
  (<Provider store={AppStore.store}>
    <ConnectedRouter history={browserHistory}>
      <Route component={AppConnect}/>
    </ConnectedRouter>
  </Provider>),
  document.getElementById('application')
);

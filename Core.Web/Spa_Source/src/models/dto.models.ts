export class OperationResult<T>
{
  public object: T;
  public success: boolean;
  public message: string;
}

export class UserDetails
{
  public id: number;
  public email: string;
  public password: string;
  public name: string;
  public isActive: boolean;
}

type GrantType = 'password' | 'refresh_token'

export class JwtRequest
{
  public grantType: GrantType;
  public refreshToken: string;
  public username: string;
  public password: string;
}

export class Authentication
{
  public accessToken: string;
  public refreshToken: string;
  public accessTokenExpires: number;
  public refreshTokenExpires: number;
  public refreshTokenExpiresDate: Date;
  public user: UserDetails;
}

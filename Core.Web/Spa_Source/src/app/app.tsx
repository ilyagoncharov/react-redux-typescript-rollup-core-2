import * as React        from 'react';
import { AppRoutes }     from '../routing/routes';
import { CSSProperties } from 'react';
import { AuthActions }   from '../store/authentication/actions';
import { connect }       from 'react-redux';
import { IAppState }     from '../store/state';
import { GetProps }      from './types';

class Loading extends React.Component
{
  public render()
  {
    const styles: CSSProperties = {
      display: 'flex',
      height: '100vh',
      alignItems: 'center',
      justifyContent: 'center',
      background: '#adbec9',
    };
    return (<div style={styles}>LOADING</div>);
  }
}

interface IStoreProps
{
  firstRefreshTokenRequestEnd: boolean;
}

type IProps = GetProps<IStoreProps>;

class App extends React.Component<IProps>
{
  constructor(props: IProps)
  {
    super(props);

    props.dispatch(AuthActions.httpRefreshToken());
  }

  public render()
  {
    return (
      (this.props.firstRefreshTokenRequestEnd)
        ? <AppRoutes/>
        : <Loading/>
    );
  }
}

const mapStateToProps = (state: IAppState): IStoreProps =>
{
  return {
    firstRefreshTokenRequestEnd: state.authentication.firstRefreshTokenRequestEnd
  };
};

export const AppConnect = connect(mapStateToProps)(App);

import * as React             from 'react';
import { UserActions }        from '../../../store/user/actions';
import { connect }            from 'react-redux';
import { IAppState }          from '../../../store/state';
import { UserDetails }        from '../../../models/dto.models';
import { HttpRequestActions } from '../../../store/http-request/actions';
import { IUserState }         from '../../../store/user/reducers';
import { GetProps }           from '../../types';

interface IStoreProps
{
  usersState: IUserState;
}

type IProps = GetProps<{}, IStoreProps>

class Users extends React.Component<IProps>
{
  constructor(props: IProps)
  {
    super(props);
  }

  public componentDidMount(): void
  {
    this.props.dispatch(UserActions.httpList());
  }

  public componentWillUnmount(): void
  {
    this.props.dispatch(UserActions.clear());
    this.props.dispatch(HttpRequestActions.abort(UserActions.httpList()));
  }

  private onChangeStatus(user: UserDetails, isActive: boolean): void
  {
    if (user.isActive !== isActive)
    {
      this.props.dispatch(UserActions.httpChangeStatus({id: user.id, isActive: isActive}));
    }
  }

  public render()
  {
    return (
      <div>
        <h3>Users</h3>
        <h3>Is Updating: {this.props.usersState.changeStatusLoading ? 'TRUE' : 'FALSE'}</h3>
        <table>
          <tbody>
            <tr>
              <th>Id</th>
              <th>Email</th>
              <th>Name</th>
              <th>Is Active</th>
            </tr>
            {
              this.props.usersState.users.map((user: UserDetails) =>
              {
                return (
                  <tr key={user.id}>
                    <td>{user.id}</td>
                    <td>{user.email}</td>
                    <td>{user.name}</td>
                    <td>
                      {
                        user.isActive
                          ? <div onClick={() => this.onChangeStatus(user, false)}>Active</div>
                          : <div onClick={() => this.onChangeStatus(user, true)}>No Active</div>
                      }
                    </td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
      </div>
    )
  }
}

const mapStateToProps = (state: IAppState): IStoreProps =>
{
  return {
    usersState: state.usersState
  };
};

export const UsersConnect = connect(mapStateToProps)(Users);

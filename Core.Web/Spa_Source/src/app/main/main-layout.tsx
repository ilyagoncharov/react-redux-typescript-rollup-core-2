import * as React      from 'react';
import { MainRoutes }  from '../../routing/routes';
import { Page }        from '../../routing/page';
import { Link }        from 'react-router-dom';
import { AuthActions } from '../../store/authentication/actions';
import { GetProps }    from '../types';
import { connect }     from 'react-redux';

class MainLayout extends React.Component<GetProps>
{
  private onLogout(): void
  {
    this.props.dispatch(AuthActions.logOut());
  }

  public render()
  {
    return (
      <div>
        <button onClick={() => this.onLogout()}>Logout</button>
        <br/>
        <br/>
        <div>
          <Link to={Page.requests.path}>Requests</Link>
          <br/>
          <Link to={Page.users.path}>Users</Link>
        </div>
        <MainRoutes/>
      </div>
    );
  }
}

export const MainLayoutConnect = connect()(MainLayout);

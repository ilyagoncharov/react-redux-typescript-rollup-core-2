import { DispatchProp } from 'react-redux';

export type GetProps<TComponentProps = {}, TStoreStoreProps = {}> =
  & TComponentProps
  & TStoreStoreProps
  & DispatchProp<any>;

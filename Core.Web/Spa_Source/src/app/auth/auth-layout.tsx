import * as React     from 'react';
import { AuthRoutes } from '../../routing/routes';

export class AuthLayout extends React.Component
{
  public render()
  {
    return (
      <div>
        Auth Layout
        <AuthRoutes/>
      </div>
    );
  }
}

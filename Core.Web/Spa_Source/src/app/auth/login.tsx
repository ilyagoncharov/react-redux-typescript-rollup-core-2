import * as React      from 'react';
import { AuthActions } from '../../store/authentication/actions';
import { GetProps }    from '../types';
import { connect }     from 'react-redux';

class Login extends React.Component<GetProps>
{
  private onLogin(): void
  {
    this.props.dispatch(AuthActions.httpLogIn({email: 'admin@itmint.ca', password: 'admin@itmint.ca'}))
  }

  public render()
  {
    return (
      <div>
        <button onClick={() => this.onLogin()}>Login</button>
      </div>
    );
  }
}

export const LoginConnect = connect()(Login);

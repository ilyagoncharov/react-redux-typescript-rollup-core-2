﻿using System;
using System.Text;
using Core.Common.AppSettings;
using Core.DAL;
using Core.DAL.Repositories;
using Core.Web.Providers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace Core.Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
            });

            services.AddOptions();
            services.Configure<AppSettingsConfig>(Configuration);
            services.AddTransient<IAppConfigurations, AppConfigurations>();

            services.AddTransient<OAuthProvider>();

            services.AddTransient<UserRepository>();
            services.AddTransient<RefreshTokenRepository>();

            using (var provider = services.BuildServiceProvider())
            {
                var config = provider.GetService<IAppConfigurations>();

                DbContextConfig.Register(services, config);

                services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidIssuer = config.Jwt.Issuer,

                            ValidateAudience = true,
                            ValidAudience = config.Jwt.Audience,

                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(config.Jwt.Secret)),

                            ValidateLifetime = true,
                            ClockSkew = TimeSpan.Zero
                        };
                    });
            }
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            DbContextConfig.Create(app, env);

            if (env.IsDevelopment())
            {
                app.UseCors(builder => builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());

                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
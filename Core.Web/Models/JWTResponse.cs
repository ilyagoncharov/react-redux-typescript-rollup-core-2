﻿using System;
using Core.DAL.Models;

namespace Core.Web.Models
{
    public class JwtResponse
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public int AccessTokenExpires { get; set; }
        public int RefreshTokenExpires { get; set; }
        public DateTime RefreshTokenExpiresDate { get; set; }
        public User User { get; set; }
    }
}

﻿namespace Core.Web.Models
{
    public class JwtRequest
    {
        public string GrantType { get; set; }
        public string RefreshToken { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL.Models;

namespace Core.DAL.Repositories
{
    public class UserRepository
    {
        private CoreContext Context { get; }

        public UserRepository(CoreContext context)
        {
            Context = context;
        }

        public User GetByEmail(string email)
        {
            return Context.Users.FirstOrDefault(x => x.Email == email);
        }

        public User GetById(long id)
        {
            return Context.Users.FirstOrDefault(x => x.Id == id);
        }

        public List<User> List()
        {
            return Context.Users.ToList();
        }

        public User Update(User user)
        {
            var entity = Context.Users.First(x => x.Id == user.Id);

            entity.IsActive = user.IsActive;

            //Context.Entry(entity).CurrentValues.SetValues(user);

            Context.SaveChanges();

            return user;
        }
    }
}

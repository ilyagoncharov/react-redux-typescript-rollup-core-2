﻿using System;
using Core.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Core.DAL
{
    public class CoreContext : DbContext
    {
        public CoreContext(DbContextOptions<CoreContext> options) : base(options)
        {
            
        }

        public DbSet<User> Users { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
    }
}

﻿using Core.Common.AppSettings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Core.DAL
{
    public static class DbContextConfig
    {
        public static void Register(IServiceCollection services, IAppConfigurations config)
        {
            var connection = config.ConnectionStrings.DefaultConnection;

            services.AddDbContext<CoreContext>(options => options.UseSqlServer(connection));
        }

        public static void Create(IApplicationBuilder app, IHostingEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<CoreContext>();
                context.Database.EnsureCreated();
            }
        }

        public static void InitSeed(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var context = services.GetRequiredService<CoreContext>();
                DbSeed.Seed(context);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL.Models;

namespace Core.DAL
{
    public static class DbSeed
    {
        public static void Seed(CoreContext context)
        {
            if (!context.Users.Any())
            {
                context.Users.Add(new User
                {
                    Email = "admin@itmint.ca",
                    Password = "admin@itmint.ca",
                    Name = "Max Truchanovich"
                });

                context.Users.Add(new User
                {
                    Email = "adminmanager1@itmint.ca",
                    Password = "adminmanager1@itmint.ca",
                    Name = "Эдуард Старжинский"
                });

                context.Users.Add(new User
                {
                    Email = "adminmanager2@itmint.ca",
                    Password = "adminmanager2@itmint.ca",
                    Name = "Влад Петров"
                });

                context.SaveChanges();
            }
        }
    }
}
